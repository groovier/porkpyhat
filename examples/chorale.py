from porkpyhat import *

allowed_intervals = (0, 4, 7)
minimum = (1, 1, 1)
maximum = (None, 1, None)
vg = VoicingGenerator(C, allowed_intervals, min_occurrences=minimum, max_occurrences=maximum)
voicings = list(vg.generate_all_voicings())
for voicing in voicings:
    print(voicing)
print(f"{len(voicings)} voicings.")
