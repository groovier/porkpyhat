from examples.chorale import voicings
from musicology.midi import MidiWrapper
from musicology.note_stack import NoteStack

midi = MidiWrapper()
midi.open_outport()

for voicing in voicings:
    chord = NoteStack.from_pitch_values(*voicing['pitches'])
    print(chord)
    chord.play()

midi.close_outport()



