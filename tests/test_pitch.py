import unittest

from porkpyhat import *


class MyTestCase(unittest.TestCase):
    def test_C4(self):
        self.assertEqual(60, pcl_to_pitch(C))

    def test_C5(self):
        self.assertEqual(72, pcl_to_pitch(C, 1))


if __name__ == '__main__':
    unittest.main()
