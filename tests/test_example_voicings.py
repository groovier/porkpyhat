import unittest

from examples import chorale, jazz


class ChoraleTestCase(unittest.TestCase):
    def test_chorale(self):
        expected = [
            {'intervals': (0, 0, 4, 7), 'pitches': [48, 60, 64, 67]},
            {'intervals': (0, 0, 7, 4), 'pitches': [48, 60, 67, 76]},
            {'intervals': (0, 4, 0, 7), 'pitches': [48, 52, 60, 67]},
            {'intervals': (0, 4, 7, 0), 'pitches': [48, 52, 55, 60]},
            {'intervals': (0, 4, 7, 7), 'pitches': [48, 52, 55, 67]},
            {'intervals': (0, 7, 0, 4), 'pitches': [48, 55, 60, 64]},
            {'intervals': (0, 7, 4, 0), 'pitches': [48, 55, 64, 72]},
            {'intervals': (0, 7, 4, 7), 'pitches': [48, 55, 64, 67]},
            {'intervals': (0, 7, 7, 4), 'pitches': [48, 55, 67, 76]}
        ]
        self.assertEqual(expected, chorale.voicings)

    def test_jazz(self):
        expected = [
            {'intervals': (0, 4, 7, 11), 'pitches': [48, 52, 55, 59]},
            {'intervals': (0, 4, 11, 7), 'pitches': [48, 52, 59, 67]},
            {'intervals': (0, 7, 4, 11), 'pitches': [48, 55, 64, 71]},
            {'intervals': (0, 7, 11, 4), 'pitches': [48, 55, 59, 64]},
            {'intervals': (0, 11, 4, 7), 'pitches': [48, 59, 64, 67]},
            {'intervals': (0, 11, 7, 4), 'pitches': [48, 59, 67, 76]}
        ]
        self.assertEqual(expected, jazz.voicings)


if __name__ == '__main__':
    unittest.main()
