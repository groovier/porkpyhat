from collections import Counter
from itertools import product

# Define pitch classes as global constants.
C = 0
D_ = 1
D = 2
E_ = 3
E = 4
F = 5
G_ = 6
G = 7
A_ = 8
A = 9
B_ = 10
B = 11


def pitch_to_pcl(pitch) -> int:
    """
    Convert a pitch to a pitch class.
    """
    return pitch % 12


def pcl_to_pitch(pcl: int, octave: int = 0) -> int:
    """
    Convert a pitch class and an octave to a pitch.

    :param pcl: pitch class represented as an int where C is 0, Db is 1, etc.
    :param octave: optional octave represented as an int where the middle piano octave is 0.

    """
    return pcl + (12 * octave) + 60


def pcl_to_pitch_above(pcl: int, floor: int, accept_identical: bool = False):
    """
    Convert a pitch class to a pitch above or equal to a certain pitch.

    :param pcl: pitch class represented as an int where C is 0, Db is 1, etc.
    :param floor: MIDI pitch which the resulting pitch must be greater than or equal to.
    :param accept_identical: if True, the returned pitch can be identical to floor.
    """
    if accept_identical:
        pitch = floor
    else:
        pitch = floor + 1
    while pitch_to_pcl(pitch) != pcl:
        pitch += 1
    return pitch


class VoicingGenerator:

    """
    :param root: the pitch class that is the root of the chord. The number 0 represents the pitch class 'C', the number
    1 represents the pitch class 'Db' (or 'C#'), etc.

    :param allowed_intervals: the intervals that can be included in the chord, expressed by their relative distances to
    root (ex.: 4 for a "Major Third"). The root note is not automatically added to the voicings - this should (if
    desired) be added by including 0 in allowed_intervals. The integers must be placed in increasing order.

    :param min_occurrences: an optional tuple with the same length as allowed_intervals that indicates the minimum
    number of occurrences of the interval with this position that a voicing must have to be valid.

    :param max_occurrences: an optional tuple with the same length as allowed_intervals that indicates the maximum
    number of occurrences of the interval with this position that a voicing must have to be valid.

    :param first_is_bottom: if True, the first interval in allowed_interval must be used as the bottom note.

    :param last_is_top: if True, the last interval in allowed_interval must be used as the top note.

    :param: voice_count: the number of voices in the voicing.

    :param lowest: the MIDI note number of the lowest possible voicing note.
    """
    def __init__(
            self,
            root: int,
            allowed_intervals: tuple,
            min_occurrences: tuple = None,
            max_occurrences: tuple = None,
            first_is_bottom: bool = True,
            last_is_top: bool = False,
            voice_count: int = 4,
            lowest: int = 48
    ):
        # Check that intervals are in increasing order
        i = 1
        while i < (len(allowed_intervals)):
            if not allowed_intervals[i] > allowed_intervals[i - 1]:
                raise ValueError(f"Intervals do not have increasing values: {allowed_intervals}.")
            i += 1

        self.root = root
        self.voice_count = voice_count
        self.lowest = lowest
        self.allowed_intervals = allowed_intervals
        self.first_is_bottom = first_is_bottom
        self.last_is_top = last_is_top
        if min_occurrences:
            self.min_occurrences = min_occurrences
        else:
            self.min_occurrences = (0,) * len(allowed_intervals)
        print(f"Minimum occurrences: {self.min_occurrences}")
        if max_occurrences:
            self.max_occurrences = max_occurrences
        else:
            self.max_occurrences = (None,) * len(allowed_intervals)
        print(f"Maximum occurrences: {self.max_occurrences}")

    def interval_permutations(self):
        return product(self.allowed_intervals, repeat=self.voice_count)

    def generate_all_voicings(self):
        for intervals_tuple in self.interval_permutations():

            # Check that bottom and top notes have allowed values.
            if self.first_is_bottom and (intervals_tuple[0] != self.allowed_intervals[0]):
                print(f"Bottom interval {intervals_tuple[0]} is not {self.allowed_intervals[0]}.")
                continue
            if self.last_is_top and (intervals_tuple[-1] != self.allowed_intervals[-1]):
                print(f"Top interval {intervals_tuple[-1]} is not {self.allowed_intervals[-1]}.")
                continue

            passed = True  # Until otherwise is proofed
            occurrences_counter = Counter(intervals_tuple)
            print()
            print(f"intervals_tuple {intervals_tuple} contains:")
            for i in range(len(self.allowed_intervals)):
                interval = self.allowed_intervals[i]
                print(f"- {occurrences_counter[interval]} occurence(s) of {interval}")
                if occurrences_counter[interval] < self.min_occurrences[i]:
                    print(f"(which is too few since there must be at least {self.min_occurrences[i]})")
                    passed = False
                    break
                if self.max_occurrences[i] and (occurrences_counter[interval] > self.max_occurrences[i]):
                    print(f"(which is too many since there cannot be more than {self.max_occurrences[i]})")
                    passed = False
                    break
            
            if passed:
                voicing = dict()
                voicing['intervals'] = intervals_tuple
                voicing['pitches'] = list()
                floor = self.lowest
                first_voice = True
                for interval in intervals_tuple:
                    pitch_class = self.root + interval
                    pitch = pcl_to_pitch_above(pitch_class, floor, first_voice)
                    voicing['pitches'].append(pitch)
                    floor = pitch
                    first_voice = False
                yield voicing

